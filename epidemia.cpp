/***************************************************************************************
*
*   Program do modelowania rozprzestrzeniania się epidemii na kwadratowej siatce.
*  
*	Autor: Dominik Kasprowicz
*	Ostatnia aktualizacja: 28 kwietnia 2020
*
***************************************************************************************/
//  g++ epidemia.cpp -std=c++14
#include <iostream>
#include <fstream>
#include <vector>
#include <list>
#include <iterator> // std::distance()
#include <random>
#include <initializer_list>
#include <string>
#include <ctime> // jadro liczb lsowych

// Zbiór możliwych stanów miasto.osobniki[i]a
enum Stan : char
{
    zaszczepiony,
    ozdrowialy,
    podatny,
    chory,
    zakazony,
    martwy
};
/********************************************************************************/

// Położenie osobnika na mapie
struct Koordynaty
{
    int x, y;

    Koordynaty() : x(0), y(0) {}

    Koordynaty(int ax, int ay) : x(ax), y(ay) {}

    Koordynaty sasiad_lewy() const { return Koordynaty(x - 1, y); }

    Koordynaty sasiad_prawy() const { return Koordynaty(x + 1, y); }

    Koordynaty sasiad_gorny() const { return Koordynaty(x, y - 1); }

    Koordynaty sasiad_dolny() const { return Koordynaty(x, y + 1); }

    // friend std::ostream &operator<<(std::ostream &ostr, const Koordynaty &wsp)
    // {
    //     ostr << "(" << wsp.x << ", " << wsp.y << ")";
    // }
};
/********************************************************************************/

struct Wirus
{
    float beta, gamma, start_tworzenia_odpornosci, moblinosc_spoleczenstwa, smiertelnosc, wykrywalnosc, social_distancing;
    int dlugosc_choroby, czas_do_odpornosci;

    Wirus() : beta(0.5), gamma(0.5) {}
    Wirus(float b, float g) : beta(b), gamma(g) {}
    Wirus(float b, float g, float s, int c, float m, float smi, float w, int dc, float sd) : beta(b), gamma(g), start_tworzenia_odpornosci(s), czas_do_odpornosci(c), moblinosc_spoleczenstwa(m), smiertelnosc(smi), wykrywalnosc(w), dlugosc_choroby(dc), social_distancing(sd)
    {
        // std::cout << "Wirus " << dlugosc_choroby << " " << dc << std::endl;
    }

    // friend std::ostream &operator<<(std::ostream &ostr, const Wirus &wirus)
    // {
    //     ostr << "Zaraza o parametrach: beta=" << wirus.beta << ", gamma=" << wirus.gamma << std::endl;
    // }
};
/********************************************************************************/

// Generator liczb pseudolosowych
class RNG
{
    std::mt19937_64 generator;
    std::uniform_int_distribution<int> losowa_koordynata;
    std::uniform_real_distribution<float> losowa_0_1;

public:
    RNG(int bok_mapy) : generator(std::time(0)), losowa_koordynata(0, bok_mapy - 1), losowa_0_1(0, 1) {}

    Koordynaty losuj_koordynaty() { return Koordynaty(losowa_koordynata(generator), losowa_koordynata(generator)); }
    float losuj_od_0_do_1() { return losowa_0_1(generator); }
};

/********************************************************************************/

struct Osobnik
{
    Stan stan;
    float gestosc_zarazkow;
    int dnie_do_odporności;
    int dnie_do_wyzdrowienia;
    Wirus wirus;
    Koordynaty koordynaty;
    bool ma_kwarantanne;

    Osobnik(Wirus w) : gestosc_zarazkow(0), dnie_do_odporności(99), dnie_do_wyzdrowienia(99), stan(Stan::podatny), wirus(w), ma_kwarantanne(false){
                                                                                                                                 // std::cout << "Osobnik:wirus " << w.dlugosc_choroby << std::endl;
                                                                                                                             };
    Osobnik(Stan s, Wirus &w) : gestosc_zarazkow(0), dnie_do_odporności(99), dnie_do_wyzdrowienia(99), stan(s), wirus(w), ma_kwarantanne(false){};

    void ustawKoordynaty(Koordynaty &wsp)
    {
        koordynaty = wsp;
    }
    void dodaj_zarazki(float nowe_zarazki)
    {
        // std::cout << "DODAJ ZARAZKI - start "
        //           << "{stan:" << stan << ", gestosc_zarazkow:" << gestosc_zarazkow << ", dnie_do_odporności:" << dnie_do_odporności << ", dnie_do_wyzdrowienia:" << dnie_do_wyzdrowienia << ", ma_kwarantanne:" << ma_kwarantanne << ", koordynaty_x:" << koordynaty.x << ", koordynaty_y:" << koordynaty.y << "}" << std::endl;

        if (stan == Stan::podatny)
        {
            stan = Stan::zakazony;
        }
        if (stan == Stan::zakazony)
        {
            gestosc_zarazkow += nowe_zarazki;
            // sprawdzamy czy dana osoba powinna zachorowac, wtedy startujemy licznik dni choroby
            if (gestosc_zarazkow >= 1)
            {
                stan = Stan::chory;
                dnie_do_wyzdrowienia = wirus.dlugosc_choroby;
                gestosc_zarazkow = 1;
            }
            // jezeli przekroczyl granice odpornosci to wystartuj licznik dni do uzyskania odpornosci
            else if (gestosc_zarazkow >= wirus.start_tworzenia_odpornosci && dnie_do_odporności == 99)
            {
                dnie_do_odporności = wirus.czas_do_odpornosci;
            }
        }
        // std::cout << "DODAJ ZARAZKI - done "
        //           << "{stan:" << stan << ", gestosc_zarazkow:" << gestosc_zarazkow << ", dnie_do_odporności:" << dnie_do_odporności << ", dnie_do_wyzdrowienia:" << dnie_do_wyzdrowienia << ", ma_kwarantanne:" << ma_kwarantanne << ", koordynaty_x:" << koordynaty.x << ", koordynaty_y:" << koordynaty.y << "}" << std::endl;
    }

    void przejdz_jeden_dzien(RNG &rng)
    {
        // std::cout << "JEDEN DZIEN 111111"
        //           << "{stan:" << stan << ", gestosc_zarazkow:" << gestosc_zarazkow << ", dnie_do_odporności:" << dnie_do_odporności << ", dnie_do_wyzdrowienia:" << dnie_do_wyzdrowienia << ", ma_kwarantanne:" << ma_kwarantanne << ", koordynaty_x:" << koordynaty.x << ", koordynaty_y:" << koordynaty.y << "}" << std::endl;
        if (stan == Stan::chory)
        {
            // std::cout << "JEDEN DZIEN - start chory"
            //           << "{stan:" << stan << ", gestosc_zarazkow:" << gestosc_zarazkow << ", dnie_do_odporności:" << dnie_do_odporności << ", dnie_do_wyzdrowienia:" << dnie_do_wyzdrowienia << ", ma_kwarantanne:" << ma_kwarantanne << ", koordynaty_x:" << koordynaty.x << ", koordynaty_y:" << koordynaty.y << "}" << std::endl;
            dnie_do_wyzdrowienia -= 1;
            if (dnie_do_wyzdrowienia <= 0)
            {
                stan = Stan::ozdrowialy;
                gestosc_zarazkow = 0;
            }

            if (rng.losuj_od_0_do_1() <= wirus.wykrywalnosc)
            {
                ma_kwarantanne = true;
            }

            if (rng.losuj_od_0_do_1() <= wirus.smiertelnosc)
            {
                stan = Stan::martwy;
                // std::cout << "martwy"
                //           << "{stan:" << stan << ", gestosc_zarazkow:" << gestosc_zarazkow << ", dnie_do_odporności:" << dnie_do_odporności << ", dnie_do_wyzdrowienia:" << dnie_do_wyzdrowienia << ", ma_kwarantanne:" << ma_kwarantanne << ", koordynaty_x:" << koordynaty.x << ", koordynaty_y:" << koordynaty.y << "}" << std::endl;
            }
            // std::cout << "JEDEN DZIEN - done chory"
            //           << "{stan:" << stan << ", gestosc_zarazkow:" << gestosc_zarazkow << ", dnie_do_odporności:" << dnie_do_odporności << ", dnie_do_wyzdrowienia:" << dnie_do_wyzdrowienia << ", ma_kwarantanne:" << ma_kwarantanne << ", koordynaty_x:" << koordynaty.x << ", koordynaty_y:" << koordynaty.y << "}" << std::endl;
        }
        else if (stan == Stan::zakazony && dnie_do_odporności != 99)
        {
            // std::cout << "JEDEN DZIEN - start zakazony"
            //           << "{stan:" << stan << ", gestosc_zarazkow:" << gestosc_zarazkow << ", dnie_do_odporności:" << dnie_do_odporności << ", dnie_do_wyzdrowienia:" << dnie_do_wyzdrowienia << ", ma_kwarantanne:" << ma_kwarantanne << ", koordynaty_x:" << koordynaty.x << ", koordynaty_y:" << koordynaty.y << "}" << std::endl;
            dnie_do_odporności -= 1;
            if (dnie_do_odporności <= 0)
            {
                stan = Stan::zaszczepiony;
                gestosc_zarazkow = 0;
                // std::cout << "szczepienie"
                //           << "{stan:" << stan << ", gestosc_zarazkow:" << gestosc_zarazkow << ", dnie_do_odporności:" << dnie_do_odporności << ", dnie_do_wyzdrowienia:" << dnie_do_wyzdrowienia << ", ma_kwarantanne:" << ma_kwarantanne << ", koordynaty_x:" << koordynaty.x << ", koordynaty_y:" << koordynaty.y << "}" << std::endl;
            }
            // std::cout << "JEDEN DZIEN - done zakazony"
            //           << "{stan:" << stan << ", gestosc_zarazkow:" << gestosc_zarazkow << ", dnie_do_odporności:" << dnie_do_odporności << ", dnie_do_wyzdrowienia:" << dnie_do_wyzdrowienia << ", ma_kwarantanne:" << ma_kwarantanne << ", koordynaty_x:" << koordynaty.x << ", koordynaty_y:" << koordynaty.y << "}" << std::endl;
        }

        //czy dana osoba zostala wykryta i przykazana do kwarantanny
    }

    void zmien_stan(Stan s)
    {
        stan = s;

        switch (s)
        {
        case Stan::chory:
            gestosc_zarazkow = 1;
            dnie_do_wyzdrowienia = wirus.dlugosc_choroby;
            // std::cout << "chory"
            //           << "{stan:" << stan << ", gestosc_zarazkow:" << gestosc_zarazkow << ", dnie_do_odporności:" << dnie_do_odporności << ", dnie_do_wyzdrowienia:" << dnie_do_wyzdrowienia << ", ma_kwarantanne:" << ma_kwarantanne << ", koordynaty_x:" << koordynaty.x << ", koordynaty_y:" << koordynaty.y << "}" << std::endl;
            break;
        case Stan::podatny:
            gestosc_zarazkow = 0;
            // std::cout << "podatny"
            //           << "{stan:" << stan << ", gestosc_zarazkow:" << gestosc_zarazkow << ", dnie_do_odporności:" << dnie_do_odporności << ", dnie_do_wyzdrowienia:" << dnie_do_wyzdrowienia << ", ma_kwarantanne:" << ma_kwarantanne << ", koordynaty_x:" << koordynaty.x << ", koordynaty_y:" << koordynaty.y << "}" << std::endl;
            break;
        case Stan::ozdrowialy:
            gestosc_zarazkow = 0;
            ma_kwarantanne = false;
            // std::cout << "ozdrowialy"
            //           << "{stan:" << stan << ", gestosc_zarazkow:" << gestosc_zarazkow << ", dnie_do_odporności:" << dnie_do_odporności << ", dnie_do_wyzdrowienia:" << dnie_do_wyzdrowienia << ", ma_kwarantanne:" << ma_kwarantanne << ", koordynaty_x:" << koordynaty.x << ", koordynaty_y:" << koordynaty.y << "}" << std::endl;
            break;
        case Stan::zaszczepiony:
            gestosc_zarazkow = 0;
            // std::cout << "zaszczepiony"
            //           << "{stan:" << stan << ", gestosc_zarazkow:" << gestosc_zarazkow << ", dnie_do_odporności:" << dnie_do_odporności << ", dnie_do_wyzdrowienia:" << dnie_do_wyzdrowienia << ", ma_kwarantanne:" << ma_kwarantanne << ", koordynaty_x:" << koordynaty.x << ", koordynaty_y:" << koordynaty.y << "}" << std::endl;
            break;
        case Stan::zakazony: // SHOULDNT happen
            throw std::invalid_argument("zakazenie moze tylko sie stac przez dodanie zarazkow");
            break;
        default:
            // std::cout << "default"
            //           << "{stan:" << stan << ", gestosc_zarazkow:" << gestosc_zarazkow << ", dnie_do_odporności:" << dnie_do_odporności << ", dnie_do_wyzdrowienia:" << dnie_do_wyzdrowienia << ", ma_kwarantanne:" << ma_kwarantanne << ", koordynaty_x:" << koordynaty.x << ", koordynaty_y:" << koordynaty.y << "}" << std::endl;
            break;
        }
    }
};

/********************************************************************************/

// Populacja jako kwadratowa siatka osobników (a właściwie ich stanów) z przydatnymi metodami.
class Populacja
{
    int bok_mapy; // Długość boku siatki
    // Generator liczb pseudolosowych używany do:
    // 1. losowania współrzędnych osobników zaszczepionych i zarażonych pierwszego dnia,
    // 2. decydowania, czy sąsiad zostanie zarażony (co zachodzi z prawdopodobieństwem beta),
    // 3. decydowania, czy osobnik wyzdrowiał (co zachodzi z prawdopodobieństwem gamma).
    RNG rng;

    // Populacja jako dwuwymiarowa siatka osobników

    // Pomocnicze listy współrzędnych: osobniki chore i odporne
    // std::list<Koordynaty> chorzy_x_y;
    // std::list<Koordynaty> odporni_x_y;
    // std::list<Koordynaty> zakazeni_x_y;

    // Parametry epidemii
    Wirus wirus;

    bool jest_na_mapie(const Koordynaty &wsp) const
    {
        return wsp.x >= 0 and wsp.y >= 0 and wsp.x < bok_mapy and wsp.y < bok_mapy;
    }

public:
    std::vector<Osobnik> osobniki;
    Populacja(int n) : bok_mapy(n), rng(bok_mapy)
    { /*reset(); */
    }

    void reset(Wirus &w)
    {
        wirus = w;
        osobniki = std::vector<Osobnik>(bok_mapy * bok_mapy, wirus);
        // chorzy_x_y.clear();
        // odporni_x_y.clear();
        // zakazeni_x_y.clear();
        for (int i = 0; i < bok_mapy * bok_mapy; i++)
        {
            // osobniki[i] = Osobnik(wirus);
            Koordynaty wsp = Koordynaty(i / bok_mapy, i % bok_mapy);
            osobniki[i].ustawKoordynaty(wsp);
        }
    }

    int dlugosc_boku() const { return bok_mapy; }

    long liczebnosc() const { return osobniki.size(); }

    // long ilu_chorych() const { return chorzy_x_y.size(); }

    // long ilu_odpornych() const { return odporni_x_y.size(); }

    // long ilu_zakazonych() const { return zakazeni_x_y.size(); }

    // long ilu_podatnych() const { return liczebnosc() - ilu_chorych() - ilu_odpornych(); } // troszke to jest niezgodne z prawda w nocym modelu

    Stan odczytaj_stan(const Koordynaty &wsp) const { return osobniki[bok_mapy * wsp.x + wsp.y].stan; }

    Osobnik odczytaj_osobnika(const Koordynaty &wsp) const { return osobniki[bok_mapy * wsp.x + wsp.y]; }

    void podmien_osobnika(const Koordynaty &wsp, Osobnik os)
    {
        osobniki[bok_mapy * wsp.x + wsp.y] = os;
    }

    void ustaw_stan(const Koordynaty &wsp, const Stan &stan)
    {

        osobniki[bok_mapy * wsp.x + wsp.y].zmien_stan(stan);
    }

    // void ustaw_osobnika(const Koordynaty &wsp, const Osobnik &osobnik) { osobniki[bok_mapy * wsp.x + wsp.y] = osobnik; }

    bool czy_chory(const Koordynaty &wsp) const { return odczytaj_stan(wsp) == Stan::chory; }

    bool czy_zaszczepiony(const Koordynaty &wsp) const { return odczytaj_stan(wsp) == Stan::zaszczepiony; }

    bool czy_ozdrowialy(const Koordynaty &wsp) const { return odczytaj_stan(wsp) == Stan::ozdrowialy; }

    bool czy_podatny(const Koordynaty &wsp) const { return odczytaj_stan(wsp) == Stan::podatny; }

    bool czy_zakazony(const Koordynaty &wsp) const { return odczytaj_stan(wsp) == Stan::zakazony; }

    bool czy_niepodatny(const Koordynaty &wsp) const { return czy_zaszczepiony(wsp) or czy_ozdrowialy(wsp); }

    // void zamien_osobniki(const Koordynaty &wsp1, const Koordynaty &wsp2) // to jest zle w obecnym algorytmie bo musimy zamieniac calego osobnika
    // {
    //     Stan stan1 = odczytaj_stan(wsp1);
    //     ustaw_stan(wsp1, odczytaj_stan(wsp2));
    //     ustaw_stan(wsp2, stan1);
    // }

    // Zwraca listę Koordynat zawierających wyłącznie podatnych sąsiadów
    std::list<Koordynaty> znajdz_podatnych_sasiadow(const Koordynaty &wsp)
    {
        std::list<Koordynaty> sasiedzi;
        for (Koordynaty sasiad : {
                 wsp.sasiad_lewy(),
                 wsp.sasiad_prawy(),
                 wsp.sasiad_dolny(),
                 wsp.sasiad_gorny()})
            if (jest_na_mapie(sasiad) and (czy_podatny(sasiad) or czy_zakazony(sasiad))) // dodanie zakazonych
                sasiedzi.push_back(sasiad);
        return sasiedzi;
    }

    // Zaszczep losowo wybrane osobniki i zainfekuj inne, również wybrane losowo.
    void zaraza_przybywa(const Wirus &wir, long ilu_chorych, long ilu_odpornych)
    {
        wirus = wir;

        // Losowe współrzędne osobników odpornych.
        for (int i = 0; i < ilu_odpornych; ++i)
        {
            ustaw_stan(rng.losuj_koordynaty(), Stan::zaszczepiony);
            // odporni_x_y.push_back(rng.losuj_koordynaty());
        }

        // Losowe współrzędne osobników zarażonych (wyłącznie wśród podatnych).
        for (int i = 0; i < ilu_chorych; ++i)
        {
            Koordynaty wsp;
            do
            {
                wsp = rng.losuj_koordynaty();
            } while (not czy_podatny(wsp));
            ustaw_stan(wsp, Stan::chory);
            //  zakazeni_x_y.push_back(wsp);
        }

        // Na początku dnia zero odporność wynika tylko z zaszczepienia.
        // for (Koordynaty &koordynaty : odporni_x_y)
        //     ustaw_stan(koordynaty, Stan::zaszczepiony);

        // // Ogniska zarazy.
        // for (Koordynaty &koordynaty : zakazeni_x_y)
        //     ustaw_stan(koordynaty, Stan::chory);
    }

    // Kolejna "tura" symulacji: chorzy mają szansę wyzdrowieć, podatni mogą się zarazić.
    void kolejny_dzien()
    {

        // std::list<Koordynaty> zakarzeni_nowi;
        std::vector<Osobnik> osobniki_po_zakazaniach(osobniki);

        // Dla każdego chorego...
        // for (Koordynaty &chory_x_y : chorzy_x_y) // TODO dla kazdego chorego i zakazonego
        for (Osobnik osobnik : osobniki) // TODO dla kazdego chorego i zakazonego - w koncu dla kazdego
        {
            if (!osobnik.ma_kwarantanne)
            {
                switch (osobnik.stan)
                {
                case Stan::chory:
                case Stan::zakazony:
                    // ...znajdujemy jego podatnych sąsiadów...
                    for (Koordynaty &sasiad_x_y : znajdz_podatnych_sasiadow(osobnik.koordynaty)) // TODO zmienic na znajdz podatnych lub zakazonych
                    {
                        Osobnik sasiad = osobniki_po_zakazaniach[bok_mapy * sasiad_x_y.x + sasiad_x_y.y]; //odczytaj_osobnika(sasiad_x_y);
                        // ... i kademu z sasiadow losujemy jak duzo zarazkow wysylamy danego dnia od chorego obok niego
                        float intensywnosc_kontaktu = rng.losuj_od_0_do_1(); // tutaj by sie przydal rozklad w ksztalcie U, pare osob z ktorymi sie non stop widujemy (rodzina) i pare osob z ktorymi mamy minimalny kontakt
                        // biorac pod uwage sile "social distancing"
                        float ilosc_przekazanych_zarazkow = osobnik.gestosc_zarazkow * intensywnosc_kontaktu * wirus.social_distancing;
                        sasiad.dodaj_zarazki(ilosc_przekazanych_zarazkow);
                        osobniki_po_zakazaniach[bok_mapy * sasiad_x_y.x + sasiad_x_y.y] = sasiad;
                        // podmien_osobnika(sasiad_x_y, sasiad);
                    }
                    break;
                case Stan::podatny:
                case Stan::ozdrowialy:
                case Stan::zaszczepiony:
                default:
                    break;
                }
            }
        }
        osobniki = osobniki_po_zakazaniach;
        for (Osobnik &osobnik : osobniki) // TODO dla kazdego chorego i zakazonego - w koncu dla kazdego
        {
            osobnik.przejdz_jeden_dzien(rng);
        }
    }

    // Zapisuje stan siatki do pliku o podanej nazwie,
    bool zapisz_do_pliku(std::ofstream plik) const
    {

        char stan_jako_znak;

        // Stan stan;
        // float gestosc_zarazkow;
        // int dnie_do_odporności;
        // int dnie_do_wyzdrowienia;
        // Wirus wirus;
        // Koordynaty koordynaty;
        // bool ma_kwarantanne;

        for (Osobnik osobnik : osobniki) // TODO dla kazdego chorego i zakazonego - w koncu dla kazdego
        {
            plik << "{stan:" << osobnik.stan << ", gestosc_zarazkow:" << osobnik.gestosc_zarazkow << ", dnie_do_odporności:" << osobnik.dnie_do_odporności << ", dnie_do_wyzdrowienia:" << osobnik.dnie_do_wyzdrowienia << ", ma_kwarantanne:" << osobnik.ma_kwarantanne << ", koordynaty_x:" << osobnik.koordynaty.x << ", koordynaty_y:" << osobnik.koordynaty.y << "}," << std::endl;
        }
        // for (int x = 0; x < bok_mapy; ++x)
        // {
        //     for (int y = 0; y < bok_mapy; ++y)
        //     {
        //         Stan stan = osobniki[bok_mapy * x + y].stan;
        //         Osobnik osobnik = osobniki[bok_mapy * x + y].stan;
        //         switch (stan)
        //         {
        //         case Stan::martwy:
        //             stan_jako_znak = '5';
        //             break;
        //         case Stan::zakazony:
        //             stan_jako_znak = '4';
        //             break;
        //         case Stan::chory:
        //             stan_jako_znak = '3';
        //             break;
        //         case Stan::podatny:
        //             stan_jako_znak = '2';
        //             break;
        //         case Stan::ozdrowialy:
        //             stan_jako_znak = '1';
        //             break;
        //         case Stan::zaszczepiony:
        //             stan_jako_znak = '0';
        //             break;
        //         default:
        //             break;
        //         }
        //         plik << stan_jako_znak << '\t';
        //     }
        //     plik << std::endl;
        // }
        return true;
    }
};
/********************************************************************************/

// Kontener do przechowywania dziennych liczności grupy w wybranym stanie, np. zarażonych.
// Udostępnia również podstawowe metody do obróbki statystycznej
// (oczywiście zachęcamy do dodawania własnych).
class Statystyka
{
    // Której grupy dotyczy statystyka: podatnych, zarażonych itp.
    Stan stan;
    std::list<long> grupa;

public:
    Statystyka(const Stan &s) : stan(s) {}

    Stan dotyczy_stanu() const { return stan; }

    void dodaj_dzisiejsze_dane(long ilu) { grupa.push_back(ilu); }

    // Zmiana liczności danej grupy ostatniego dnia.
    long ile_dzisiaj_nowych() const
    {
        if (grupa.size() == 0)
            return 0;
        if (grupa.size() == 1)
            return grupa.back();
        auto ostatni = --grupa.end();
        return *ostatni - *(--ostatni);
    }

    // Maksymalna liczba osobników w danej grupie w czasie trwania eksperymentu.
    long maksimum() const
    {
        auto szczytowy_dzien = grupa.begin();
        for (auto dzis = grupa.begin(); dzis != grupa.end(); ++dzis)
        {
            if (*dzis > *szczytowy_dzien)
                szczytowy_dzien = dzis;
        }
        return *szczytowy_dzien;
    }

    // Na który dzień przypadł szczyt liczności grupy
    int kiedy_maksimum() const
    {
        auto szczytowy_dzien = grupa.begin();
        for (auto dzis = grupa.begin(); dzis != grupa.end(); ++dzis)
        {
            if (*dzis > *szczytowy_dzien)
                szczytowy_dzien = dzis;
        }
        return std::distance(grupa.begin(), szczytowy_dzien);
    }

    // Zlicz dni, w których liczność grupy przekracza podaną wartość
    int ile_dni_powyzej(long ilu) const
    {
        long ile_dni = 0;
        for (auto dzis = grupa.begin(); dzis != grupa.end(); ++dzis)
            if (*dzis > ilu)
                ++ile_dni;
        return ile_dni;
    }

    // Od którego dnia liczność grupy się (aż do końca symulacji)
    // poniżej podanej wartości
    int od_kiedy_ponizej(long ilu) const
    {
        // Będziemy się cofać, poczynając od ostatniego dnia
        auto dzis = grupa.rbegin();
        for (; dzis != grupa.rend(); ++dzis)
            if (*dzis >= ilu)
                break;
        return std::distance(grupa.rbegin(), dzis);
    }

    void wypisz() const
    {
        for (auto x : grupa)
            std::cout << x << '\t';
        std::cout << std::endl;
    }

    // Zapisuje dane z całego eksperymentu do pliku o podanej nazwie,
    // oddzielając poszczególne rekordy znakiem tabulacji i kończąc
    // znakiem nowego wiersza.
    // Jeśli drugi argument to 'true', nadpisuje plik.
    bool zapisz_do_pliku(const std::string &nazwa_pliku, bool nadpisz = false) const
    {
        std::ofstream plik;

        if (nadpisz)
            plik.open(nazwa_pliku);
        else
            plik.open(nazwa_pliku, std::ostream::app);

        if (not plik.is_open())
        {
            std::cout << " Nie mogę utworzyć pliku '" << nazwa_pliku << "`" << std::endl;
            return false;
        }
        for (auto x : grupa)
            plik << x << '\t';
        plik << std::endl;

        plik.close();
        return true;
    }
};
/********************************************************************************/

int main()
{
    // Pierwiastek z liczby osobników (bok kwadratowej siatki).
    // Nie należy bać się liczb rzędu 100 (tysiąca), choć
    // ciekawe ciekawe wyniki można uzyskać i dla 100.
    const int bok_mapy = 100;

    // Liczba osobników zarażonych na początku epidemii.
    const long chorzy_dnia_zero[] = {10};

    // Liczba osobników zaszczepionych przed nastaniem epidemii.
    const long zaszczepieni_dnia_zero[] = {0};

    // Prawdopodobieństwo zarażenia każdego z sąsiadów
    // danego osobnika w jednostce czasu.
    const float beta = 0.5; // - NIE POTRZEBNE JUZ

    // Prawdopodobieństwo wyzdrowienia w jednostce czasu.
    const float gamma = 0.25; // - NIE POTRZEBNE JUZ

    // NOWE parametry

    // biorac pod uwage, ze przy 100% jednostek wirusa atakujacych dana osobe ulega ona zakazeniu
    // zeta to poziom procentowy minimalny od ktorego cialo zaczyna sie uodparniać na działanie wirusa
    const float start_tworzenia_odpornosci[] = {1}; //{0.1, 0.5, 1};
    // eta to czas w dniach jaki organizm potrzbuje przebywać pomiędzy poziomem wirusow zeta, a 100% (bez staniem sie zakazonym), by uzyskac odpowiedz uodpornienia
    const int czas_do_odpornosci[] = {4}; //{4, 7};
    // mobilność osobnikow w populacji w %
    const float moblinosc_spoleczenstwa[] = {1}; // TODO nie uzyty jeszcze
    // smiertelnosc choroby
    const float smiertelnosc[] = {0.01}; //{0.01, 0.05, 0.1};
    // % wykrywalnosci co pozwala nalozyc scislal kwarantanne na dana osobe
    const float wykrywalnosc[] = {0.01}; // {0.3, 0.5, 0.8};
    // czas trwania choroby
    const int dlugosc_choroby[] = {14};
    // social distancing
    const float social_distancing[] = {0.4}; //{0.3, 0.6};

    // Liczba niezależnych (!) eksperymentów Monte Carlo.
    const int ile_eksperymentow = 1;

    // Ile dni trwa pojedynczy eksperyment.
    const int ile_dni = 120;

    // Jedno miasto posłuży nam do całej serii eksperymentów Monte Carlo.
    Populacja miasto(bok_mapy);

    std::ofstream plik("epidemia/public/dane.json");

    if (not plik.is_open())
    {
        std::cout << " Nie mogę utworzyć pliku 'dane.txt`" << std::endl;
    }

    plik << " [" << std::endl;

    for (int cdz = 0; cdz < sizeof(chorzy_dnia_zero) / sizeof(*chorzy_dnia_zero); ++cdz)
        for (int zdz = 0; zdz < sizeof(zaszczepieni_dnia_zero) / sizeof(*zaszczepieni_dnia_zero); ++zdz)
            for (int sto = 0; sto < sizeof(start_tworzenia_odpornosci) / sizeof(*start_tworzenia_odpornosci); ++sto)
                for (int cdo = 0; cdo < sizeof(czas_do_odpornosci) / sizeof(*czas_do_odpornosci); ++cdo)
                    for (int ms = 0; ms < sizeof(moblinosc_spoleczenstwa) / sizeof(*moblinosc_spoleczenstwa); ++ms)
                        for (int smi = 0; smi < sizeof(smiertelnosc) / sizeof(*smiertelnosc); ++smi)
                            for (int wyk = 0; wyk < sizeof(wykrywalnosc) / sizeof(*wykrywalnosc); ++wyk)
                                for (int dc = 0; dc < sizeof(dlugosc_choroby) / sizeof(*dlugosc_choroby); ++dc)
                                    for (int sd = 0; sd < sizeof(social_distancing) / sizeof(*social_distancing); ++sd)
                                    {

                                        std::cout << "start_tworzenia_odpornosci:" << start_tworzenia_odpornosci[sto]
                                                  << ",czas_do_odpornosci:" << czas_do_odpornosci[cdo]
                                                  << ",moblinosc_spoleczenstwa:" << moblinosc_spoleczenstwa[ms]
                                                  << ",smiertelnosc:" << smiertelnosc[smi]
                                                  << ",wykrywalnosc:" << wykrywalnosc[wyk]
                                                  << ",dlugosc_choroby:" << dlugosc_choroby[dc]
                                                  << ",social_distancing:" << social_distancing[sd]
                                                  << ",ile_dni:" << ile_dni
                                                  << ",bok_mapy:" << bok_mapy << std::endl;
                                        // Rozne werjse paramterow wejsciowych
                                        plik << "{\"start_tworzenia_odpornosci\":" << start_tworzenia_odpornosci[sto]
                                             << ",\"czas_do_odpornosci\":" << czas_do_odpornosci[cdo]
                                             << ",\"moblinosc_spoleczenstwa\":" << moblinosc_spoleczenstwa[ms]
                                             << ",\"smiertelnosc\":" << smiertelnosc[smi]
                                             << ",\"wykrywalnosc\":" << wykrywalnosc[wyk]
                                             << ",\"dlugosc_choroby\":" << dlugosc_choroby[dc]
                                             << ",\"social_distancing\":" << social_distancing[sd]
                                             << ",\"ile_dni\":" << ile_dni
                                             << ",\"chorzy_dnia_zero\":" << chorzy_dnia_zero[cdz]
                                             << ",\"zaszczepieni_dnia_zero\":" << zaszczepieni_dnia_zero[zdz]
                                             << ",\"bok_mapy\":" << bok_mapy
                                             << ", \"eksperymenty\":[" << std::endl;

                                        for (int eksp_nr = 0; eksp_nr < ile_eksperymentow; ++eksp_nr)
                                        {
                                            plik << "[" << std::endl;
                                            Wirus wirus(beta, gamma, start_tworzenia_odpornosci[sto], czas_do_odpornosci[cdo], moblinosc_spoleczenstwa[ms], smiertelnosc[smi], wykrywalnosc[wyk], dlugosc_choroby[dc], social_distancing[sd]);
                                            // Wirus wirus(beta, gamma);
                                            Statystyka chorzy(Stan::chory);
                                            Statystyka podatni(Stan::podatny);
                                            Statystyka ozdrowiali(Stan::ozdrowialy);

                                            // Ten krok (reset) jest konieczny! Wszyscy mieszkańcy stają się na nowo podatni.
                                            // Stan generatora liczb pseudolosowych NIE jest resetowany, więc kolejny eksperyment
                                            // będzie miał inny przebieg niż ostatni (i o to chodzi).
                                            miasto.reset(wirus);
                                            miasto.zaraza_przybywa(wirus, chorzy_dnia_zero[cdz], zaszczepieni_dnia_zero[zdz]);

                                            // Właściwy eksperyment odbywa się tu.
                                            for (int dzien = 0; dzien < ile_dni; ++dzien)
                                            {
                                                plik << "[" << std::endl;
                                                // chorzy.dodaj_dzisiejsze_dane(miasto.ilu_chorych());
                                                miasto.kolejny_dzien();
                                                for (int i = 0; i < miasto.osobniki.size(); i++) //   Osobnik osobnik : miasto.osobniki) // TODO dla kazdego chorego i zakazonego - w koncu dla kazdego
                                                {
                                                    plik << "{\"stan\":" << miasto.osobniki[i].stan << ", \"gestosc_zarazkow\":" << miasto.osobniki[i].gestosc_zarazkow << ", \"dnie_do_odporności\":" << miasto.osobniki[i].dnie_do_odporności << ", \"dnie_do_wyzdrowienia\":" << miasto.osobniki[i].dnie_do_wyzdrowienia << ", \"ma_kwarantanne\":" << miasto.osobniki[i].ma_kwarantanne << ", \"koordynaty_x\":" << miasto.osobniki[i].koordynaty.x << ", \"koordynaty_y\":" << miasto.osobniki[i].koordynaty.y << "}";
                                                    if (i == miasto.osobniki.size() - 1)
                                                        plik << std::endl;
                                                    else
                                                        plik << "," << std::endl;
                                                }
                                                if (dzien == ile_dni - 1)
                                                    plik << "]" << std::endl;
                                                else
                                                    plik << "]," << std::endl;
                                            }
                                            if (eksp_nr == ile_eksperymentow - 1)
                                                plik << "]" << std::endl;
                                            else
                                                plik << "]," << std::endl;
                                        }
                                        plik << "]}" << std::endl;
                                    }

    plik << "]" << std::endl;

    plik.close();
}
