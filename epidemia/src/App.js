import React,{ useEffect,useState,useRef } from 'react';
import logo from './logo.svg';
import './App.css';
import createPlotlyComponent from 'react-plotly.js/factory';
const Plotly = window.Plotly;
const Plot = createPlotlyComponent(Plotly);
// import eksperymenty from './dane'


function App() {

  const [eksperymenty,setEksperymenty] = useState([{}]);
  useEffect(() => {
    fetch("./dane.json")
      .then(res => res.json())
      .then(data => { console.log(11111111,data); setEksperymenty(data); })
  },[])
  const wersja_parametrow = 0;
  let wersja_eksperymentu = 0;
  const [dzien,setDzien] = useState(0);
  const [statystyki,setStatystyki] = useState([]);
  const ile_dni = eksperymenty[wersja_parametrow].ile_dni;
  const interval = useRef(false) //now you can pass timer to another component
  const miasto = eksperymenty[wersja_parametrow].eksperymenty ? eksperymenty[wersja_parametrow].eksperymenty[wersja_eksperymentu][dzien] : null;

  useEffect(() => {
    interval.current = setInterval(() => {
      setDzien(dzien => dzien < ile_dni - 1 ? dzien + 1 : dzien);
    },1000);

    return () => {
      clearInterval(interval.current);
    };
  },[ile_dni]);

  useEffect(() => {
    const wszystkieDnie = eksperymenty[wersja_parametrow].eksperymenty ? eksperymenty[wersja_parametrow].eksperymenty[wersja_eksperymentu] : [];

    const newStats = wszystkieDnie.map((dzien) =>
      dzien.reduce((stat,person) => ({
        ...stat,
        [person.stan]: stat[person.stan] ? stat[person.stan] + 1 : 1
      }),{})
    )
    console.log('stats ',newStats)
    setStatystyki(newStats);
  },[eksperymenty])






  console.log(222222,eksperymenty[0]);


  if(!miasto) {
    return <div>Wczytuje...</div>
  }


  const color = (state,ma_kwarantanne) => {
    switch(state) {
      case 0:
        return 'green'; //zaszczepiony
      case 1:
        return 'blue'; //ozdrowialy
      case 2:
        return 'white'; // podatny
      case 3:
        return ma_kwarantanne ? 'darkred' : 'red'; //chory
      case 4:
        return 'pink'; //zakazony
      case 5:
        return 'dimgray'; //martwy
      default:
        return '';
    }
  }
  console.log(miasto);

  let table = [];
  const bok_mapy = eksperymenty[0].bok_mapy;
  for(let i = 0; i < bok_mapy; i++) {
    table = [...table,miasto.slice(i * bok_mapy,((i + 1) * bok_mapy))]
  }

  console.log(table);
  const tiles = table.map(row => {

    return (
      <div style={{ display: 'table-row' }}>
        {
          row.map(sample => (
            <div style={{ backgroundColor: color(sample.stan,sample.ma_kwarantanne),display: 'table-cell',fontSize: 'medium',width: 5,height: 20 }}>
              {/* {sample.gestosc_zarazkow.toFixed(2)} */}
            </div>
          ))
        }
      </div>
    )
  });


  return (
    <div className="App">
      {/* <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Troszkę inny model epidemii.
        </p>
      </header> */}
      <div style={{ margin: 50,float: 'left' }}>
        <div style={{ width: 200,float: 'left',display: 'inline-block' }}>Dzień symulacji: <b>{dzien}</b></div>
        <div style={{ width: 200,float: 'left',display: 'inline-block' }}>Liczba dni do odporności: <b>{eksperymenty[wersja_parametrow].czas_do_odpornosci}</b></div>
        <div style={{ width: 200,float: 'left',display: 'inline-block' }}>Liczba dni trwania choroby: <b>{eksperymenty[wersja_parametrow].dlugosc_choroby}</b></div>
        <div style={{ width: 200,float: 'left',display: 'inline-block' }}>Prawdopodobieństwo śmierci: <b>{eksperymenty[wersja_parametrow].smiertelnosc}</b></div>
        <div style={{ width: 200,float: 'left',display: 'inline-block' }}>Poziom interakcji osobników (przeciwieństwo social distancing): <b>{eksperymenty[wersja_parametrow].social_distancing}</b></div>
        <div style={{ width: 200,float: 'left',display: 'inline-block' }}>Poziom zarazków do rozpoczecia tworzenia odporności: <b>{eksperymenty[wersja_parametrow].start_tworzenia_odpornosci}</b></div>
        <div style={{ width: 200,float: 'left',display: 'inline-block' }}>Prawdopodobieństwo wykrycia choroby: <b>{eksperymenty[wersja_parametrow].wykrywalnosc}</b></div>
      </div>
      <div style={{ margin: 50,clear: 'both' }}>
        <span style={{ padding: 20,backgroundColor: 'white' }}>podatny</span>
        <span style={{ padding: 20,backgroundColor: 'green' }}>zaszczepiony</span>
        <span style={{ padding: 20,backgroundColor: 'pink' }}>zakazony</span>
        <span style={{ padding: 20,backgroundColor: 'red' }}>chory</span>
        <span style={{ padding: 20,backgroundColor: 'darkred' }}>chory z kwarantanna</span>
        <span style={{ padding: 20,backgroundColor: 'blue' }}>ozdrowialy</span>
        <span style={{ padding: 20,backgroundColor: 'dimgray' }}>martwy</span>

      </div>

      <div style={{ display: 'table',tableLayout: 'fixed',width: 1800 }}>
        {tiles}
      </div>
      <span style={{ display: 'inline-block' }}>

        <Plot
          data={[
            {
              y: statystyki.map(dzienneStats => dzienneStats[0]),
              x: [...Array(ile_dni).keys()],
              type: 'scatter',
              name: 'Zaszczepieni',
              marker: { color: 'green' },
            },
            {
              y: statystyki.map(dzienneStats => dzienneStats[1]),
              x: [...Array(ile_dni).keys()],
              type: 'scatter',
              name: 'Ozdrowiali',
              marker: { color: 'blue' },
            },
            {
              y: statystyki.map(dzienneStats => dzienneStats[2]),
              x: [...Array(ile_dni).keys()],
              type: 'scatter',
              name: 'Podatny',
              marker: { color: 'white' },
            },
            {
              y: statystyki.map(dzienneStats => dzienneStats[3]),
              x: [...Array(ile_dni).keys()],
              type: 'scatter',
              name: 'Chory',
              marker: { color: 'red' },
            },
            {
              y: statystyki.map(dzienneStats => dzienneStats[4]),
              x: [...Array(ile_dni).keys()],
              type: 'scatter',
              name: 'Zakazony',
              marker: { color: 'pink' },
            },
            {
              y: statystyki.map(dzienneStats => dzienneStats[5]),
              x: [...Array(ile_dni).keys()],
              type: 'scatter',
              name: 'Martwy',
              marker: { color: 'dimgray' },
            }
          ]}
          layout={{
            title: 'Czasy przekształcenia/algorytmu [ms]',
            autosize: true,
            width: 1500,
            height: 500,

          }}
        />
      </span>
    </div>
  );
}

export default App;
